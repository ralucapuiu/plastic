function craigslistSearch(searchQuery) {
    var url = 'https://toronto.craigslist.ca/search/ggg?query=' + searchQuery + '&is_paid=all';

    $.get(url, function (html) {
        var parsedHtml = $.parseHTML(html);
        var rows = $(parsedHtml).find('.result-row');

        var extractedRows = [];

        $.each(rows, function(index, li) {
            var extractedRow = new Object();

            var title = $(li).find('.result-title').text();
            var link = $(li).find('.result-title').attr('href');
            var date = $(li).find('time').attr('datetime');
            var location = $.trim($(li).find('.result-hood').text());
            var imgData = $(li).find('.result-image.gallery').attr('data-ids');
            var image = null;
            if(imgData) {
                if(split = imgData.split(':')) {
                    image = 'https://images.craigslist.org/' + split[1] + '_300x300.jpg';
                }
            }

            extractedRow.title = title;
            extractedRow.link = link;
            extractedRow.date = date;
            extractedRow.location = location;
            extractedRow.image = image;

            extractedRows.push(extractedRow);
        });

        $('#result').html(JSON.stringify(extractedRows));
    })
}